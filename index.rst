.. title:: Payreto OPP

Payreto OPP's documentation!
============================

Payreto OPP is a PHP library that makes easy to integrate with Open Payment Platform.

User Guide
==========

.. toctree::
    :maxdepth: 2

    implements_logger
    brands
    amex
    diners
    discover
    giropay
    jcb
    klarna_installments
    klarna_invoice
    master
    paydirekt
    paypal
    ratenkauf
    sepa
    sofortuberweisung
    visa
    backoffice
    report
    copy_and_pay_prepare_checkout
    copy_and_pay_create_payment_form
    copy_and_pay_get_payment_status
    copy_and_pay_tokenization_store_during_payment_merchant_prepare_checkout
    copy_and_pay_tokenization_store_as_stand_alone_prepare_checkout
    copy_and_pay_tokenization_store_as_stand_alone_get_registration_result
    copy_and_pay_one_click_checkout_prepare_checkout
    copy_and_pay_recurring_prepare_checkout
    sync_server_to_server_initial_payment
    async_server_to_server_initial_payment
    async_server_to_server_redirect
    async_server_to_server_payment_status
    server_to_server_tokenization_store_during_payment
    server_to_server_tokenization_store_as_stand_alone
    server_to_server_tokenization_delete
    server_to_server_one_click_payment_checkout
    server_to_server_one_click_payment_send
    sync_server_to_server_recurring_initial_payment
    sync_server_to_server_recurring_repeated_payment
