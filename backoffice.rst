Backoffice
##########

Capture an authorization
************************

We can capture a previously authorize funds by using the ``payment.id``. Example:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentId = '8a82944a5d2c3545015d3491d76749a1';

    $parameters = array(
        'amount' => '10.00',
        'currency' => 'EUR',
    );

    $result = $opp->backoffice()->capture($paymentId, $parameters);

    // $result = '{"id":"8a82944a5d2c3545015d3491d76749a1","paymentType":"CP","amount":"10.00","currency":"EUR","descriptor":"4136.1163.5362 OPP_Channel","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Wirecard Test"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-12 02:12:39+0000","ndc":"8a8294174b7ecb28014b9699220015ca_34a6d26334db4bcf981fae9e31af3c1c"}';

Refund a payment
****************

A refund is performed against a previous payment, using the ``payment.id``. Example:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentId = '8a82944a5d2c3545015d34960b014caa';

    $parameters = array(
        'amount' => '10.00',
        'currency' => 'EUR',
    );

    $result = $opp->backoffice()->refund($paymentId, $parameters);

    // $result = '{"id":"8a82944a5d2c3545015d34960b014caa","paymentType":"RF","amount":"10.00","currency":"EUR","descriptor":"8559.9326.6850 OPP_Channel","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Wirecard Test"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-12 02:17:14+0000","ndc":"8a8294174b7ecb28014b9699220015ca_7cf4ae9b14b34ff2aedeafba1957583d"}';

Reverse a payment
*****************

Reversal can be done using :php:meth:`Payreto\\Opp\\Services\\Backoffice::reversal` method by passing the ``payment.id`` from previous payment:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentId = '8a82944a5d2c3545015d349c1c284ed5';

    $result = $opp->backoffice()->reversal($paymentId);

    // $result = '{"id":"8a82944a5d2c3545015d349c1c284ed5","paymentType":"RV","amount":"92.00","currency":"EUR","descriptor":"7311.7077.9810 OPP_Channel","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Wirecard Test"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-12 02:23:52+0000","ndc":"8a8294174b7ecb28014b9699220015ca_67e5d0e6c9ed4f13b080af99fa1e691a"}';
