Server-to-Server: One-Click Payment: Send the Payment
#####################################################

When the customer checks out with One-Click, all you need to do is send a server-to-server request for a new payment with the respective ``registration.id`` (token) for which you want to make the payment. Example:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $registrationId = '8a82944a5d2c3545015d3151550b3583';

    $result = $opp->syncServerToServer()->oneClickPayment($registrationId, array(
        'amount' => '92.00',
        'currency' => 'EUR',
        'paymentType' => 'DB',
    ));

    // $result = '{"id":"8a82944a5d2c3545015d316c60454edd","paymentType":"DB","amount":"92.00","currency":"EUR","descriptor":"3306.3991.3634 CC_3D ","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"ConnectorTxID3":"235379","ExtendedDescription":"There were no errors during the execution of the operation.","ConnectorTxID2":"b75b6159-c2ec-45f7-b16c-eeca478d1f4c","AcquirerResponse":"0","ConnectorTxID1":"20692109","Bank Message":"Approved"},"risk":{"score":"0"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 11:32:52+0000","ndc":"8a8294174d0a8edd014d0a9f97430077_6e935db2b900415681b5874aad9e9635"}';
