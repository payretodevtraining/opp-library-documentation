Server-to-Server: Tokenization: Delete Stored Payment Data
##########################################################

Once stored, a token can be deleted using the ``registration.id``:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $registrationId = '8a82944a5d2c3545015d3151550b3583';

    $result = $opp->syncServerToServer()->deleteRegistrationId($registrationId);

    // $result = '{"id":"8a82944a5d2c3545015d316f7fb35217","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 11:36:17+0000","ndc":"8a8294174d0a8edd014d0a9f97430077_0dc4f58dddcc41ffaa3f6cd3c02c42cd"}';
