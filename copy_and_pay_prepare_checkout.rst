COPYandPAY: Prepare the checkout
################################

First, perform a server-to-server POST request to prepare the checkout with the required data, including the order type, amount and currency. The response to a successful request is a JSON string with an ``id``, which is required in the second step to create the payment form.

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $parameters = array(
        'amount' => '92.00',
        'currency' => 'EUR',
        'paymentType' => 'DB',
    );

    $result = $opp->copyAndPay()->prepareCheckout($parameters);

    // $result = '{"result":{"code":"000.200.100","description":"successfully created checkout"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 08:44:05+0000","ndc":"02F3CC224E1D7270475D7F07033B68CD.sbg-vm-tx02","id":"02F3CC224E1D7270475D7F07033B68CD.sbg-vm-tx02"}';
