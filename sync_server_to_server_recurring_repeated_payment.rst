Server-to-Server: Recurring: Sending the repeated payment
#########################################################

After we have the shopper's ``registrationId``, now we can send repeated payment:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $registrationId = '8a82944a5d2c3545015d3156887a3b28';

    $result = $opp->syncServerToServer()->payRecurringly($registrationId, array(
        'paymentType' => 'PA',
        'amount' => '10.00',
        'currency' => 'EUR',
    ));

    // $result = '{"id":"8a8294495d2c4bcd015d315da83e3bf9","paymentType":"PA","amount":"92.00","currency":"EUR","descriptor":"8903.2783.9394 OPP_Channel ","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Elavon-euroconex_UK_Test"},"risk":{"score":"100"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 11:16:47+0000","ndc":"8a8294174b7ecb28014b9699220015ca_8a327ccab2bf4bb6b1cea1ad7a5d1a75"}';
