Server-to-Server: Recurring: Sending the initial payment
########################################################

If the shopper has no ``registrationId`` for recurring, then we must send an initial recurring payment like this:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentBrandRequiredParameters = array(
        'paymentType' => 'DB',
        'amount' => '10.00',
        'currency' => 'EUR',
        'paymentBrand' => 'VISA',
        'card.number' => '4200000000000000',
        'card.holder' => 'Jane Jones',
        'card.expiryMonth' => '05',
        'card.expiryYear' => '2018',
        'card.cvv' => '123',
        'recurringType' => 'INITIAL',
        'createRegistration' => 'true',
    ); // This is just example for VISA, you must follow the Payment Brand's required parameters.

    $result = $opp->syncServerToServer()->pay($paymentBrandRequiredParameters);

    // $result = '{"id":"8a8294495d2c4bcd015d31568b0e3437","registrationId":"8a82944a5d2c3545015d3156887a3b28","paymentType":"DB","paymentBrand":"VISA","amount":"10.00","currency":"EUR","descriptor":"0388.5057.2962 CC_3D ","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"ConnectorTxID3":"235379","ExtendedDescription":"There were no errors during the execution of the operation.","ConnectorTxID2":"b75b6159-c2ec-45f7-b16c-eeca478d1f4c","AcquirerResponse":"0","ConnectorTxID1":"20692109","Bank Message":"Approved"},"card":{"bin":"420000","last4Digits":"0000","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"customParameters":{"OPP_card.bin":"420000"},"risk":{"score":"0"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 11:09:01+0000","ndc":"8a8294174d0a8edd014d0a9f97430077_308e1617f8a9421189c50ed653d36918"}';

Then, save the ``registrationId`` for repeated payment.
