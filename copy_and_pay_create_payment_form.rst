COPYandPAY: Create the payment form
###################################

Create the payment form by using the ``checkoutId`` and specify the ``shopperResultUrl``. Example:

.. code-block:: php

    <?php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $shopperResultUrl = 'http://yourshop.com/payment/callback';

    $checkoutId = '02F3CC224E1D7270475D7F07033B68CD.sbg-vm-tx02'; // From previous step...

    ?>

    <!-- $shopperResultUrl is the page on your site where the customer should be redirected to after the payment is processed -->
    <form action="<?php echo $shopperResultUrl; ?>" class="paymentWidgets" data-brands="VISA MASTER AMEX"></form>

    <!-- $checkoutId is id from previous step -->
    <script src="<?php echo $opp->copyAndPay()->paymentWidgetJs($checkoutId); ?>"></script>
