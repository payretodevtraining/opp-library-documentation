Brands
######

* :doc:`amex`
* :doc:`diners`
* :doc:`discover`
* :doc:`giropay`
* :doc:`jcb`
* :doc:`klarna_installments`
* :doc:`klarna_invoice`
* :doc:`master`
* :doc:`paydirekt`
* :doc:`paypal`
* :doc:`ratenkauf`
* :doc:`sepa`
* :doc:`sofortuberweisung`
* :doc:`visa`
